#!/usr/bin/python

import os
import sys

left_to_right = False
possible_ext = ['.jpg', '.bmp', '.png']
remove_source_if_splitted = True

if len(sys.argv) >= 2:
    source_root = sys.argv[1]
else:
    source_root = '.'

def split_file_name(file_path):
    parent, base_name = os.path.split(file_path)
    if parent != "":
        parent += os.sep
    dot_index = base_name.rfind('.')
    if dot_index == -1:
        file_name = base_name
        ext_name = ''
    else:
        file_name = base_name[:dot_index]
        ext_name = base_name[dot_index:]
    return parent, file_name, ext_name

def get_image_size(image_path):
    a = os.popen('identify ' + image_path, 'r').read()
    a = a.replace('\t', ' ')
    splitted = a.split(' ')
    if len(splitted) < 3:
        return 0,0

    splitted = splitted[2].split('x')
    if len(splitted) < 2:
        return 0,0

    return int(splitted[0]), int(splitted[1])
    
    


def save_half(image_path, out_path, is_left = True):
    if is_left:
        os.system('convert ' + image_path + ' -crop 50x100%+0+0 +repage ' + out_path)
    else:
        os.system('convert ' + image_path + ' -flop -crop 50x100%+0+0 -flop +repage ' + out_path )

def split_image(image_path):
    parent, file_name, ext_name = split_file_name(image_path)
    out1 = parent + file_name + '_a' + ext_name
    out2 = parent + file_name + '_b' + ext_name

    if not left_to_right:
        out1, out2 = out2, out1

    width, height = get_image_size(image_path)
    #print(width, height)
    if width < height:
        return

    if not os.path.exists(out1):
        save_half(image_path, out1, True)
    if not os.path.exists(out2):
        save_half(image_path, out2, False)

    if remove_source_if_splitted:
        os.remove(image_path)

    return True

def is_output_file(file_name):
    if (file_name.find('_a.') >= 0 or file_name.find('_b.') >= 0):
        return True
    else:
        return False

def is_image(file_name):
    for ext in possible_ext:
        if file_name.endswith(ext):
            return True

    return False

count = 0
for root, dirs, files in os.walk(source_root):
    for f in files:
        if is_image(f) and not is_output_file(f):
            if split_image(root + os.sep + f):
                print(count, ':', root + os.sep + f)
                count += 1


