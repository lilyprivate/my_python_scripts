#!/usr/bin/python

"""
Purpose: 
  Compare app_conf_android.json and app_conf_iphone.json and show difference between their /pages/* and /partials/*.

When, where, who:
  201505182151~2236, Changping of Beijing, James Bear(jamesgrizzly@gmail.com)

Environment:
  OS X 10.9.1 + python3.4.2

Path when written:
  /Users/bear/svn/Server_Ruby/dev-trunk/diff_app_conf.py
"""

import json

SOURCE1 = 'public/app_conf_android.json'
SOURCE2 = 'public/app_conf_iphone.json'

def short_desc(obj):
    desc = ''
    if obj == None:
        desc = 'None'
    elif isinstance(obj, dict):
        desc = 'dict(' + str(len(obj)) + ')'
    elif isinstance(obj, list):
        desc = 'list(' + str(len(obj)) + ')'
    else:
        desc = str(obj)
    return desc

def compare_tree(tree1, tree2):
    if tree1 == None and tree2 != None:
        return [['', short_desc(None), short_desc(tree2)]]
    elif tree1 != None and tree2 == None:
        return [['', short_desc(tree1), short_desc(None)]]
    elif tree1 == None and tree2 == None:
        return []

    if isinstance(tree1, dict) and isinstance(tree2, dict):
        t2 = tree2.copy()
        diff = []
        for k,v in tree1.items():
            v2 = t2.get(k)
            if v2 != None:
                t2.pop(k)
            d1 = compare_tree(v, v2)
            for i in range(len(d1)):
                item = d1[i]
                item[0] = '/' + k + item[0]
                d1[i] = item
            diff.extend(d1)
        return diff
    elif tree1 != tree2:
        return [['', short_desc(tree1), short_desc(tree2)]]
    else:
        return []

def load_json(json_path):
    f = open(json_path)
    j = json.load(f)
    return j

def load_two_jsons():
    j1 = load_json(SOURCE1)
    j2 = load_json(SOURCE2)
    return j1, j2

def print_result(result_list):
    for i in result_list:
        print(i)

def main():
    j1, j2 = load_two_jsons()
    p1, p2 = j1['pages'], j2['pages']
    d1 = compare_tree(p1, p2)
    print('DIFF FOR PAGES:')
    print_result(d1)
    print('-'*20)
    print('DIFF FOR PARTIALS:')
    par1, par2 = j1['partials'], j2['partials']
    d2 = compare_tree(par1, par2)
    print_result(d2)

main()
